augroup vimrc | autocmd! | augroup END

call plug#begin()

Plug 'tpope/vim-sensible'

Plug 'NLKNguyen/papercolor-theme'
Plug 'mhinz/vim-startify'

Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sleuth'
Plug 'justinmk/vim-ipmotion'
Plug 'AndrewRadev/splitjoin.vim'

Plug 'lifepillar/vim-mucomplete'

Plug 'lervag/vimtex'

if exists('$OPAM_SWITCH_PREFIX')
  Plug $OPAM_SWITCH_PREFIX . '/share/merlin', { 'rtp': 'vim' }
endif

call plug#end()

" Early load vim-sensible to override its settings.
runtime START plugin/sensible.vim

let g:PaperColor_Theme_Options =
  \ { 'theme': { 'default': { 'allow_italic': 1 } } }
set background=light
color PaperColor

set hidden

set shiftwidth=2 expandtab
set laststatus=0 noruler noshowcmd

set noswapfile nobackup nowritebackup noundofile
let g:netrw_dirhistmax = 0
set viminfo^=<0

nnoremap Y y$
vnoremap > >gv
vnoremap < <gv

" Setup vim-startify.
let g:startify_custom_header = 'startify#center([execute("version")->matchstr("[^\n]\\{-1,}\\ze *[(\n]")])'

" Setup vim-ipmotion.
let g:ip_skipfold = v:true

" Setup vimtex.
let g:vimtex_view_method = 'skim'
let g:vimtex_view_skim_sync = v:true

" Setup code completion.
set completeopt=menu,menuone,noselect
set shortmess+=c
let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#can_complete = {}
let g:mucomplete#can_complete.tex =
  \ { 'omni': { t -> t =~# g:vimtex#re#neocomplete . '$' } }

" Setup cursor shape.
let &t_SI = "\e[6 q"
let &t_SR = "\e[4 q"
let &t_EI = "\e[2 q"

" Enable italic text for Apple Terminal.
if $TERM_PROGRAM ==# 'Apple_Terminal'
  let &t_ZH = "\e[3m"
  let &t_ZR = "\e[23m"
endif
